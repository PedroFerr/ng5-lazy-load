import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LazyParent2Component } from './lazy-parent-2.component';

describe('LazyParent2Component', () => {
  let component: LazyParent2Component;
  let fixture: ComponentFixture<LazyParent2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LazyParent2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LazyParent2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
