import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lazy-child-2',
  templateUrl: './lazy-child-2.component.html',
  styleUrls: ['./lazy-child-2.component.css']
})
export class LazyChild2Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
