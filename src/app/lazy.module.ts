import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LazyParentComponent } from './lazy-parent/lazy-parent.component';
import { LazyChildComponent } from './lazy-child/lazy-child.component';
// And a 2nd pair parent/child also lazy loaded:
import { LazyParent2Component } from './lazy-parent-2/lazy-parent-2.component';
import { LazyChild2Component } from './lazy-child-2/lazy-child-2.component';

// The RouterModule calls forChild(routes).
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    { path: 'load-me', component: LazyParentComponent },
    { path: 'any-other-path', component: LazyParent2Component }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes)

    ],
    declarations: [LazyParentComponent, LazyChildComponent, LazyParent2Component, LazyChild2Component]
})
export class LazyModule { }
